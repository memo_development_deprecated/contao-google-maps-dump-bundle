# Memo Map Dump Bundle

## About

## Installation

tbd

## Usage

tbd

## Features

tbd

## Requirements

* PHP 7.3+
* Contao 4.4+ (tested on Contao 4.9, some features only work with Contao 4.9)

## License

[GNU Lesser General Public License, Version 3.0](https://www.gnu.de/documents/lgpl-3.0.de.html)
