<?php declare(strict_types=1);

/**
 * @package   Memo\GoogleMapDumpBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

$GLOBALS['TL_DCA']['tl_module']['palettes']['google_map_listing'] = '{title_legend}, name, headline, type;{config_legend},googleMapDumpCCM19,googleMapDumpCCM19Group, googlemaps_map, imgSize; {template_legend}, customTpl, googleMapDumpTemplate;{expert_legend:hide},cssID';

$GLOBALS['TL_DCA']['tl_module']['fields']['googleMapDumpTemplate'] = [
    'exclude' => true,
    'inputType' => 'select',
    'options_callback' => static function () {
        return \Contao\Controller::getTemplateGroup('google_map_item');
    },
    'eval' => array('chosen' => true, 'tl_class' => 'w50'),
    'sql' => "varchar(64) NOT NULL default ''"

];

$GLOBALS['TL_DCA']['tl_module']['fields']['googleMapDumpCCM19'] = [

    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['googleMapDumpCCM19'],
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class' => 'w50 clr m12'),
    'sql'                     => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_module']['fields']['googleMapDumpCCM19Group'] = [
    'label'                   => &$GLOBALS['TL_LANG']['tl_module']['googleMapDumpCCM19Group'],
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'text',
    'eval'                    => array('maxlength'=>255, 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
];
