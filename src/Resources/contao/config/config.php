<?php declare(strict_types=1);

/**
 * @package   Memo\MemoGoogleMapDumpBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Add front end modules
 */
array_insert($GLOBALS['FE_MOD'], 9, array
(
    'mod_google_map_listing' => [
        'google_map_listing' => 'Memo\GoogleMapDumpBundle\Module\ModuleGoogleMapDumpListing',
    ]
));

