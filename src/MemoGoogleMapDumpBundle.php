<?php declare(strict_types=1);

/**
 * @package   Memo\MemoGoogleMapDumpBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\GoogleMapDumpBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MemoGoogleMapDumpBundle extends Bundle
{
}
