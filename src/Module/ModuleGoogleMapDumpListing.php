<?php declare(strict_types=1);

/**
 * @package   Memo\MemoGoogleMapDumpBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\GoogleMapDumpBundle\Module;

use Contao\System;
use HeimrichHannot\GoogleMapsBundle\Manager\MapManager;
use HeimrichHannot\GoogleMapsBundle\Model\GoogleMapModel;
use HeimrichHannot\GoogleMapsBundle\Model\OverlayModel;
use Memo\FoundationBundle\Module\FoundationModule;

class ModuleGoogleMapDumpListing extends FoundationModule
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_google_map_listing';

    protected function compile()
    {
        if (TL_MODE == 'FE') {

            // Get all selected Archives
            $colItems = OverlayModel::findBy(['pid=?', 'published=?'], [$this->googlemaps_map, 1]);
            $objArchive = GoogleMapModel::findByPk($this->googlemaps_map);

            $this->foundation_item_template = 'google_map_item';

            if ($this->googleMapDumpTemplate) {
                $this->foundation_item_template = $this->googleMapDumpTemplate;
            } else {
                $this->foundation_item_template = 'google_map_item';
            }

            $this->mapManager = System::getContainer()->get('huh.google_maps.map_manager');
            $arrMapSettings = $this->mapManager->prepareMap(intval($this->googlemaps_map));

            $arrDataMapOptions = $arrMapSettings['mapModel']->getMapOptions();

            if ($arrMapSettings['mapModel']->getCenter()) {
                $arrDataMapOptions['center'] = [];
                $arrDataMapOptions['center']['lat'] = $arrMapSettings['mapModel']->getCenter()->getLatitude();
                $arrDataMapOptions['center']['lng'] = $arrMapSettings['mapModel']->getCenter()->getLongitude();
            }

            $this->Template->dataMapOptions = json_encode($arrDataMapOptions);

            if (is_object($colItems)) {
                $arrItems = $this->parseItems($colItems);
                $this->Template->items = $arrItems;
            }
            if(isset($this->googleMapDumpCCM19) && $this->googleMapDumpCCM19 == 1) {
                $GLOBALS['TL_HEAD'][] = '<script type="text/x-ccm-loader" data-ccm-loader-group="'.$this->googleMapDumpCCM19Group.'" data-ccm-loader-src="https://maps.googleapis.com/maps/api/js?key=' . MapManager::computeApiKey($objArchive) . '"></script>';
            }else{
                $GLOBALS['TL_JAVASCRIPT'][] = 'https://maps.googleapis.com/maps/api/js?key=' . MapManager::computeApiKey($objArchive) . '';
            }

            if ($this->customTpl) {
                $this->Template->strTemplate = $this->customTpl;
            }

            $this->Template->parse();
        } else {
            // Parse BackendTemplate
            $this->parseBackendTemplate();
        }
    }

    public function parseItem($objItem, $arrArchive = false, $strDefaultLanguage = false, $bolAddDetailLinkToImage = true, $strClass = '', $intCount = 0, $arrAllCategories)
    {
        $objItem->dataMapItem = json_encode($objItem->row());

        return parent::parseItem($objItem, $arrArchive, $strDefaultLanguage, $bolAddDetailLinkToImage, $strClass, $intCount, $arrAllCategories);
    }
}
